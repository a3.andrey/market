<?php

namespace Database\Factories;

use App\Models\Category;
use App\Services\ImagePlaceholder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GameFactory extends Factory
{
    const DEFAULT_IMAGE = 'https://via.placeholder.com/420x280/258DC8/E0F6FD';
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $image = new ImagePlaceholder();
        return [
            //
            'name' => fake()->name(),
            'image' => $image->put(),
        ];
    }
}

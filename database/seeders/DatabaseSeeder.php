<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AdminUser;
use App\Models\Category;
use App\Models\Game;
use App\Models\GameTag;
use App\Models\Item;
use App\Models\ItemFeaturesColor;
use App\Models\ItemFeaturesIcon;
use App\Models\ItemFeaturesNumber;
use App\Models\ItemFeaturesText;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        if(!AdminUser::where('email','admin@domain.com')->first()){
            \App\Models\AdminUser::factory()->create([
                'name' => 'admin',
                'email' => 'admin@domain.com',
                'password' => Hash::make('password'),
            ]);
        }

        if(!User::where('email','user@user.com')->first()){
            \App\Models\User::factory()->create([
                'nickname' => 'test',
                'email' => 'user@user.com',
                'password' => Hash::make('password'),
            ]);
        }

//            \App\Models\GameTag::factory()->create([
//                'id' => 1,
//                'name' => 'Игры',
//            ]);
//            \App\Models\GameTag::factory()->create([
//                'id' => 2,
//                'name' => 'Приложения',
//            ]);
//            \App\Models\GameTag::factory()->create([
//                'id' => 3,
//                'name' => 'Аккаунты',
//            ]);
//            \App\Models\GameTag::factory()->create([
//                'id' => 4,
//                'name' => 'Услуги',
//            ]);
//            \App\Models\GameTag::factory()->create([
//                'id' => 5,
//                'name' => 'Валюта',
//            ]);

//            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//            DB::table('game_tags')->truncate();
//            DB::table('games')->truncate();
//            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//
//            \App\Models\Game::factory()->create([
//                'name' => 'Roblox Adopt me',
//                'image' => '/img/catalogIndex/Roblox Adopt me.jpg',
//            ]);
//            \App\Models\Game::factory()->create([
//                'name' => 'Roblox Pet Simulator X',
//                'image' => '/img/catalogIndex/Roblox Pet Simulator X.jpg',
//            ]);
//            \App\Models\Game::factory()->create([
//                'name' => 'Roblox MM2 (Murder and Mystery 2)',
//                'image' => '/img/catalogIndex/Roblox MM2 (Murder and Mystery 2).jpg',
//            ]);

//            foreach (Game::all() as $game){
//                $game->tags()->attach([1,3]);
//            }

        if(\App\Models\Category::all()->count() < 5){
            \App\Models\Category::factory()->create([
                'id' => 1,
                'name' => 'Игры',
            ]);
            \App\Models\Category::factory()->create([
                'id' => 2,
                'name' => 'Приложения',
            ]);
            \App\Models\Category::factory()->create([
                'id' => 3,
                'name' => 'Аккаунты',
            ]);
            \App\Models\Category::factory()->create([
                'id' => 4,
                'name' => 'Услуги',
            ]);
            \App\Models\Category::factory()->create([
                'id' => 5,
                'name' => 'Валюта',
            ]);
        }



//       $item = Item::factory(5)
//            ->state(new Sequence(
//                ['game_id' => Game::all()->random()->id],
//            ))
//           ->state(new Sequence(
//               ['category_id' => Category::all()->random()->id],
//           ))
//           ->has(
//               ItemFeaturesIcon::factory(mt_rand(0,5)),'iconFeatures'
//           )
//           ->has(
//               ItemFeaturesText::factory(mt_rand(0,5)),'textFeatures'
//           )
//           ->has(
//               ItemFeaturesNumber::factory(mt_rand(0,5)),'numberFeatures'
//           )
//           ->has(
//               ItemFeaturesColor::factory(mt_rand(0,5)),'colorFeatures'
//           );
//
//
//        if(Item::all()->count() > 0){
//            $item->state(new Sequence(
//                ['parent_id' => Item::all()->random()->id],
//            ));
//        }
//
//        $item->create();

    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item_features_icons', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('icon');

            $table->bigInteger('item_id')
                ->unsigned()
                ->index()
                ->nullable();
            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item_features_icons');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('avatars', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->timestamps();
        });

        $path = '/img/avatars/';

        DB::table('avatars')->insert([
            ['image' => $path.'9.jpeg'],
            ['image' => $path.'1.jpg'],
            ['image' => $path.'2.jpeg'],
            ['image' => $path.'3.jpg'],
            ['image' => $path.'4.jpeg'],
            ['image' => $path.'5.jpg'],
            ['image' => $path.'6.jpg'],
            ['image' => $path.'7.jpg'],
            ['image' => $path.'8.png'],
            ['image' => $path.'10.webp'],
            ['image' => $path.'11.jpg'],
            ['image' => $path.'12.webp'],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('avatars')->delete();
        Schema::dropIfExists('avatars');
    }
};

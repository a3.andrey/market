<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('game_tag', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('game_id')->unsigned()->index()->nullable();
            $table->foreign('game_id')
                ->references('id')->on('games')->onDelete('cascade');

            $table->bigInteger('game_tag_id')->unsigned()->index()->nullable();
            $table->foreign('game_tag_id')
                ->references('id')->on('game_tags')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('game_tag');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unitpay_payments', function (Blueprint $table) {
            $table->id();
            $table->string('unitpay_id', 255)->unique();
            $table->string('account', 255);
            $table->double('sum');
            $table->bigInteger('items_count')->default(1);
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('restrict');
            $table->bigInteger('order_id')->unsigned()->index();
            $table->foreign('order_id')
                ->references('id')->on('orders')->onDelete('restrict');
            $table->smallInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unitpay_payments');
    }
};

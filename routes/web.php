<?php

use App\Models\Category;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('parser/market',function (){
    foreach (\App\Models\Item::all() as $item){
        $parents = $item->parents;
        foreach ($item->products as $product){
            if($product->feature_icons->count()){
                dd($product->feature_icons);
            }
        }
    }
});

Route::get('/',fn() => Inertia::render('Main',[
    'tags' => \App\Http\Resources\GameTagResource::collection(\App\Models\GameTag::all()),
    'games' => \App\Http\Resources\GameResource::collection(\App\Models\Game::all()),
    'category' => Category::getGameCat(),
    'sliders' => \App\Models\Slider::all()
]))->name('main');

Route::get('games',fn() => Inertia::render('Games',[
  'tags' => \App\Http\Resources\GameTagResource::collection(\App\Models\GameTag::all()),
  'category' => Category::getGameCat(),
  'games' => \App\Http\Resources\GameResource::collection(\App\Models\Game::all())
]))->name('games');

Route::get('search',\App\Http\Controllers\SearchController::class)->name('search');

Route::get('game/{game}/category/{category}/market',[\App\Http\Controllers\MarketController::class,'index'])
    ->name('market');

//FAVORIVE
Route::get('/item/{id}', function($id) {
    $item = \App\Models\Item::where('id', $id)->get();
    return \App\Http\Resources\ItemResource::collection($item);
})->name('getItem');


Route::get('game/{game}/category/{category}/market/{item}/item',\App\Http\Controllers\LotsController::class)
    ->name('market.item');

Route::get('posts',[\App\Http\Controllers\PostsController::class,'index'])
    ->name('posts.index');

Route::get('basket',fn() => Inertia::render('Basket'))
    ->name('basket');

Route::get('post/{post}',fn(\App\Models\Post $post) => Inertia::render('Post',[
    'post' => $post
]) )->name('posts.show');

Route::post('post/{post}/like',function(\App\Models\Post $post) {
    $post->like = (int)$post->like+1;
    $post->save();
    return $post->like;
})->name('posts.add.like');

Route::get('privacy-policy',fn() => Inertia::render('Policy') )->name('privacy-policy');

Route::get('terms-of-use',fn() => Inertia::render('TermUse') )->name('terms-of-use');

Route::get('info',fn() => Inertia::render('Info') )->name('info');

Route::get('online-buy',fn() => 1)->name('online-buy');

Route::get('product',fn() => Inertia::render('Product'))
    ->name('product');

Route::get('support',fn() => Inertia::render('Support') )->name('support');

Route::redirect('dashboard','dashboard/lots')->name('dashboard')
    ->middleware(['auth','verified']);

Route::middleware(['auth','verified'])
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {

    Route::name('lots.')->prefix('lots')->group(function () {

            Route::get('/',fn(\App\Helpers\GroupProductHelper $groupProductHelper) =>

            Inertia::render('Dashboard/Lots',[
                'lots' => $groupProductHelper->handle(
                    \Illuminate\Support\Facades\Auth::user()
                        ->products()
                        ->orderBy('id','DESC')
                        ->get()
                        ->groupBy('game_id')
                        ->groupBy('category_id')
                )
            ]))->name('index');

            Route::get('creates/{item?}/{category?}/{product?}', [\App\Http\Controllers\ProductController::class,'create'])
                ->name('create');

            Route::get('edit/{product}',[\App\Http\Controllers\ProductController::class,'edit'])
                ->name('edit');

            Route::post('update/{product}',[\App\Http\Controllers\ProductController::class,'update'])
            ->name('update');

            Route::get('replicate/{product}',[\App\Http\Controllers\ProductController::class,'replicate'] )
                ->name('replicate');

            Route::post('store',[\App\Http\Controllers\ProductController::class,'store'])
                ->name('store');

            Route::post('{product}/delete',function(\App\Models\Product $product){
                $product->delete();
                return redirect()->back();
            })->name('delete');

            Route::get('products/items/category/game',function (){
                $items = \App\Models\Item::where('game_id',request('game'))
                    ->whereHas('categories', function($q){
                        $q->whereIn('category_id', [request('category')]);
                    })->get();

                return response()->json($items->map(function ($item){
                    return [
                        'id' => $item->id,
                        'name' => $item->name,
                        'iconFeatures' => \App\Http\Resources\IconFeatures::collection($item->iconFeatures),
                        'textFeatures' => $item->getTextFeaturesItems(),
                        'colorFeatures' => $item->colorFeatures->chunk(2),
                        'numberFeatures' => $item->numberFeatures->chunk(3),
                    ];
                }));
            })->name('category.game');

        });

    Route::get('notifications',fn() => Inertia::render('Dashboard/Notification',[
        'notifications' => \App\Models\Notification::all()
    ]) )->name('notifications');

    Route::get('pay',\App\Http\Controllers\OrderController::class)->name('pay');

    Route::get('history',fn() => Inertia::render('Dashboard/History') )->name('history');

    Route::get('chat',fn() => Inertia::render('Dashboard/Chat') )->name('chat');

    Route::get('/favorite', function(){
        return Inertia::render('Dashboard/Favorite');
    })->name('favorite');


    Route::prefix('profile')->name('profile.')->group(function () {

        Route::get('/',[\App\Http\Controllers\ProfileController::class, 'edit'])->name('edit');

        Route::post('/',[\App\Http\Controllers\ProfileController::class, 'update'])->name('update');

//        Route::get('checkIsHaveNewEmail', [\App\Http\Controllers\ProfileController::class, 'checkIsHaveNewEmail'])
//            ->name('checkIsHaveNewEmail');


        Route::post('/set-new-avatar', [\App\Http\Controllers\ProfileController::class, 'setNewAvatar'])
            ->name('set-new-avatar');

        Route::get('/editPassword', [\App\Http\Controllers\ProfileController::class, 'editPasswordView'])
            ->name('editPassword');

        Route::post('/edit-password', [\App\Http\Controllers\ProfileController::class, 'editPassword'])
            ->name('edit.password');

        Route::get('/getAvatar', function(){
            return \Illuminate\Support\Facades\Auth::user()->avatar?->image;
        })->name('getAvatar');

        Route::get('/avatars', function(){
            return \App\Models\Avatar::all();
        })->name('avatars');

        Route::get('checkIsHaveNewEmail', [\App\Http\Controllers\ProfileController::class, 'checkIsHaveNewEmail'])
            ->name('checkIsHaveNewEmail');

    });
});

Route::prefix('notifications')
    ->name('notifications.')->group(function () {

    Route::get('/count', [App\Http\Controllers\NotificationController::class, 'count'])
        ->name('count');

    Route::get('/lasts', [App\Http\Controllers\NotificationController::class, 'lasts'])
        ->name('lasts');

    Route::post('/store', [App\Http\Controllers\NotificationController::class, 'store'])
        ->name('store');

    Route::post('/check', [App\Http\Controllers\NotificationController::class, 'check'])
        ->name('check');

//    Route::get('/getNumbers', [App\Http\Controllers\NotificationController::class, 'getNumber'])
//        ->name('notifications.getNumber');

//    Route::get('/getForHeader', [App\Http\Controllers\NotificationController::class, 'getNotificationsForHeader'])->name('notifications.get.header');

//    Route::post('/send', [App\Http\Controllers\NotificationController::class, 'sendNotification'])->name('notifications.send');
//
//    Route::post('/checkNotification', [App\Http\Controllers\NotificationController::class, 'checkNotification'])->name('profile.notifications.checkNotification');

});


require __DIR__.'/auth.php';


//    Route::resource('profiles',\App\Http\Controllers\Dashboard\ProfileController::class);

//    Route::prefix('profile')->group(function (){
//        Route::get('/', [ProfileController::class, 'edit'])->name('profile.edit');
//
//        Route::post('/', [ProfileController::class, 'update'])->name('profile.update');
//
//        Route::delete('/', [ProfileController::class, 'destroy'])->name('profile.destroy');
//    });


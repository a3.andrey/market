<?php



namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Slider;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      return Inertia::render('Admin/Sliders/Index',
      [
        'sliders' => \App\Models\Slider::orderBy('id', 'DESC')->get(),
      ]
    );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
      return Inertia::render('Admin/Sliders/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        //
        $validated = \request()->validate([
            'image' => 'required',
        ]);
        if(\request()->file('image')){
            $slider = Slider::create([
                'title' => \request('title'),
                'url' => \request('url'),
                'image' => \request('image')->store('image')
            ]);
            $slider->save();
        }

        return redirect()->route('admin.sliders.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Slider $slider)
    {
        return Inertia::render('Admin/Sliders/Edit',
            [

                'slider' => $slider,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Slider $slider)
    {
        //
        $slider->update([
            'title' => \request('title'),
            'url' => \request('url'),
        ]);

        if($request->file('image')){
            $slider->image = \request('image')->store('image');
            $slider->save();
        }

        return redirect()->route('admin.sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Slider $slider)
    {
        //
        $slider->delete();
        return redirect()->back();
    }
}

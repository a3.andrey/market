<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\ItemRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\GameResource;
use App\Http\Resources\ItemResource;
use App\Models\Category;
use App\Models\Game;
use App\Models\Item;
use App\Models\ItemFeaturesColor;
use App\Models\ItemFeaturesIcon;
use App\Models\ItemFeaturesNumber;
use App\Models\ItemFeaturesText;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Inertia\Inertia;

class ItemController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return Inertia::render('Admin/Items/Index',[
            'items' => Item::orderBy('id','DESC')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return Inertia::render('Admin/Items/Create',[
            'games' => GameResource::collection(Game::all()),
            'categories' => Category::all()->pluck('name','id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ItemRequest $request)
    {

        $item = Item::create([
            'name' => $request->name,
            'game_id' => $request->game,
        ]);

        $item->categories()->attach(\request('categories'));

        if($request->image){
            $item->image = $request->image->store('items');
            $item->save();
        }

        foreach ($request->texts?:[] as $txt){
            ItemFeaturesText::create([
                'name' => Arr::get($txt,'label'),
                'value' => Arr::get($txt,'val'),
                'item_id' => $item->id,
            ]);
        }

        foreach ($request->graphic?:[] as $graphic){
            if(Arr::get($graphic,'text') &&
                Arr::get($graphic,'img')
            ){
                ItemFeaturesIcon::create([
                    'name' => Arr::get($graphic,'text'),
                    'icon' => Arr::get($graphic,'img')->store('avatars'),
                    'item_id' => $item->id,
                ]);
            }
        }

        foreach ($request->numbers ?:[]as $number){
            if(Arr::get($number,'text') &&
                Arr::get($number,'min') &&
                Arr::get($number,'max')

            ){
                ItemFeaturesNumber::create([
                    'item_id' => $item->id,
                    'name' => Arr::get($number,'text'),
                    'to' => Arr::get($number,'min'),
                    'from' => Arr::get($number,'max'),
                ]);
            }
        }

        foreach ($request->rares?:[] as $rar){
            if(Arr::get($rar,'name') &&
                Arr::get($rar,'color')
            ){
                ItemFeaturesColor::create([
                    'item_id' => $item->id,
                    'name' => Arr::get($rar,'name'),
                    'color_number' => Arr::get($rar,'color'),
                ]);
            }
        }

        return redirect()->route('admin.items.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Item $item)
    {
        //
        return Inertia::render('Admin/Items/Edit',[
            'item' => new \App\Http\Resources\Admin\ItemResource($item),
            'games' => GameResource::collection(Game::all()),
            'categories' =>  Category::all()->pluck('name','id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Item $item)
    {
        //
        $item->update([
            'name' =>\request('name'),
            'game_id' => \request('game'),
        ]);

        $item->categories()->sync(\request('categories'));
//
        if(\request('image')){
            $item->image = \request('image')->store('items');
            $item->save();
        }
//

        foreach (\request('texts')?:[] as $txt){
            if(isset($txt['id'])){
                $txtM = ItemFeaturesText::find($txt['id']);
                if($txtM){
                    $txtM->name = Arr::get($txt,'label');
                    $txtM->value =  Arr::get($txt,'val');
                }
                $txtM->save();
            }else{
                ItemFeaturesText::create([
                    'name' => Arr::get($txt,'label'),
                    'value' => Arr::get($txt,'val'),
                    'item_id' => $item->id,
                ]);
            }
        }

        foreach (\request('graphic')?:[] as $graphic){
            //Если нет id то это новый Айтем
            if(!isset($graphic['id'])){
                if($graphic['img']){
                    ItemFeaturesIcon::create([
                        'name' => Arr::get($graphic,'text'),
                        'icon' => Arr::get($graphic,'img')->store('avatars'),
                        'item_id' => $item->id,
                    ]);
                }
            }else{
                $iconM = ItemFeaturesIcon::find($graphic['id']);
                if($iconM){
                    $iconM->name = Arr::get($graphic,'text');
                    if($graphic['img']){
                        $iconM->icon =  Arr::get($graphic,'img')->store('avatars');
                    }
                    $iconM->save();
                }
            }
        }

        foreach (\request('numbers')?:[] as $number){
            if(Arr::get($number,'text') &&
                Arr::get($number,'min') &&
                Arr::get($number,'max')

            ) {
                if(isset($number['id'])){
                    $numberM = ItemFeaturesNumber::find($number['id']);
                    if($numberM){
                        $numberM->name = Arr::get($number,'text');
                        $numberM->to = Arr::get($number,'min');
                        $numberM->from = Arr::get($number,'max');
                    }
                }else{
                    ItemFeaturesNumber::create([
                        'item_id' => $item->id,
                        'name' => Arr::get($number,'text'),
                        'to' => Arr::get($number,'min'),
                        'from' => Arr::get($number,'max'),
                    ]);
                }
            }
        }

        foreach (\request('rares')?:[] as $rar){
            if(Arr::get($rar,'name') &&
                Arr::get($rar,'color')
            ){
                if(isset($rar['id'])){
                    $colorM = ItemFeaturesColor::find($rar['id']);
                    if($colorM){
                        $colorM->name = Arr::get($rar,'name');
                        $colorM->color_number = Arr::get($rar,'color');
                        $colorM->save();
                    }
                }else{
                    ItemFeaturesColor::create([
                        'item_id' => $item->id,
                        'name' => Arr::get($rar,'name'),
                        'color_number' => Arr::get($rar,'color'),
                    ]);
                }
            }

        }

        return redirect()->route('admin.items.index');


    }



    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Item $item)
    {
        //
        $item->delete();
        return redirect()->back();
    }
}

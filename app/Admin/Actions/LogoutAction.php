<?php

namespace App\Admin\Actions;

use App\Interfaces\ActionInterface;
use Illuminate\Support\Facades\Auth;

class LogoutAction implements ActionInterface
{
    public function handle(){
        Auth::guard('web')->logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect()->route('admin.login');
    }
}

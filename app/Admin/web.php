<?php
use Illuminate\Support\Facades\Route;

//************************** ADMIN **********************************
Route::middleware('guest:admin','admin_template:admin')->group(function () {

    Route::get('login',[\App\Admin\Http\Controllers\AuthenticatedSessionController::class,'create'])
        ->name('login');

    Route::post('login',[\App\Admin\Http\Controllers\AuthenticatedSessionController::class,'store']);


});
//************************** END ADMIN **********************************


Route::middleware('auth.admin:admin','admin_template:admin')->group(function () {

    Route::post('icon-features/{featuresIcon}/delete',function (\App\Models\ItemFeaturesIcon $featuresIcon){
        $featuresIcon->delete();
        return response()->json('ok');
    })->name('icon-features.delete');

    Route::post('text-features/{featuresText}/delete',function (\App\Models\ItemFeaturesText $featuresText){
        $featuresText->delete();
        return response()->json('ok');
    })->name('text-features.delete');

    Route::post('number-features/{featuresNumber}/delete',function (\App\Models\ItemFeaturesNumber $featuresNumber){
        $featuresNumber->delete();
        return response()->json('ok');
    })->name('number-features.delete');

    Route::post('color-features/{featuresColor}/delete',function (\App\Models\ItemFeaturesColor $featuresColor){
        $featuresColor->delete();
        return response()->json('ok');
    })->name('color-features.delete');

    Route::get('/',\App\Admin\Http\Controllers\DashboardController::class)->name('dashboard');

    Route::post('posts/{post}/updated',[\App\Admin\Http\Controllers\PostController::class,'update'])
        ->name('posts.updated');

    Route::resource('posts', \App\Admin\Http\Controllers\PostController::class)->only([
        'create', 'store', 'destroy','index','edit'
    ]);

    Route::resource('items', \App\Admin\Http\Controllers\ItemController::class)
        ->only([
            'create', 'store', 'destroy','index','edit'
        ]);

    Route::post('items/{item}/update',[\App\Admin\Http\Controllers\ItemController::class,'update'])
        ->name('items.update');



    Route::resource('sliders', \App\Admin\Http\Controllers\SliderController::class);

    Route::post('games/{game}/updated',[\App\Admin\Http\Controllers\GameController::class,'update'])
        ->name('games.updated');

    Route::resource('games', \App\Admin\Http\Controllers\GameController::class)->only([
        'create', 'store', 'destroy','index','edit'
    ]);

    Route::post('logout', fn(\App\Admin\Actions\LogoutAction $action) => $action->handle())
        ->name('logout');

});


<?php

namespace App\Helpers;

use App\Models\Item;

class PriceItemHelper
{
    public function handler(Item $item){
        $prices = $item->products->filter(function ($item) {
            return !is_null($item->price);
        });

        return $prices->min('price');
    }
}

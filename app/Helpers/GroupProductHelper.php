<?php

namespace App\Helpers;
use App\Http\Resources\ItemResource;
use App\Http\Resources\ProductResource;

class GroupProductHelper
{
    public function handle($products){
        $result = collect();
        foreach ($products->first()?:[] as $key=>$itemContents){
            $prod = $itemContents->first();
            if($prod){
                $gameName = $prod->game->name;
                $categoryName = $prod?->category?->name;
                $result[$gameName.' - '.$categoryName] = ProductResource::collection($itemContents);
            }
        }

        return $result ;
    }
}

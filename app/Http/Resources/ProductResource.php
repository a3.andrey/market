<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'count' => $this->count,
            'price' => $this->price,
            'price_min' => $this->price_min,
            'game' => $this->game,
            'category' => $this->category,
            'item' => $this->item,
            'features_color' => $this->features_color,
            'feature_icons' => $this->feature_icons,
            'features_texts' => $this->features_texts,
            'features_numbers' => $this->features_numbers,
        ];
    }
}

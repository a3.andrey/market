<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'iconFeatures' => \App\Http\Resources\IconFeatures::collection($this->iconFeatures) ,
            'textFeatures' => $this->textFeatures->chunk(2),
            'colorFeatures' => $this->colorFeatures->chunk(2),
            'numberFeatures' => $this->numberFeatures->chunk(3),
        ];
    }
}

<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'item' => $this->item,
            'name' => $this->name,
            'game' => $this->game,
            'price' => $this->price,
            'quantity' => $this->products->count(),
            'image' => $this->image,
            'categories' => $this->categories->pluck('id'),
            'textFeatures' => $this->textFeatures->map(fn($item) => [
                'id' => $item->id,
                'label'=> $item->name,
                'val'=> $item->value,
            ]),
            'iconFeatures' => $this->iconFeatures->map(fn($item) => [
                'id' => $item->id,
                'text' => $item->name,
                'img' => null,
                'preview' => $item->icon,
            ]),
            'colorFeatures' => $this->colorFeatures->map(fn($item) => [
                'id' => $item->id,
                'color' => $item->color_number,
                'name' => $item->name,
            ]),
            'numberFeatures' => $this->numberFeatures->map(function ($item){
                return [
                    'id' => $item->id,
                    'min' => $item->to,
                    'max' => $item->from,
                    'text' => $item->name,
                ];
            }),
        ];
    }
}

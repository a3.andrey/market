<?php

namespace App\Http\Middleware;

use App\Http\Resources\GameResource;
use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

use App\Models\User;
use App\Models\Notification;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     */
    public function version(Request $request): string|null
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        return array_merge(parent::share($request), [
            'url' =>[
                'previous' => url()->previous()
            ],
            'auth' => [
                'user' => $request->user(),
                'check' => Auth::check(),
            ],
            'flash' => [
                'message' => fn () => $request->session()->get('message')
            ],
            'ziggy' => fn () => array_merge((new Ziggy)->toArray(), [
                'location' => $request->url(),
            ]),
            'popular_games' => [
                [
                    'link' => '#',
                    'name' => 'Roblox'
                ],
                [
                    'link' => '#2',
                    'name' => 'Roblox2'
                ]
            ],
            'games' => GameResource::collection(Game::all())
        ]);
    }
}

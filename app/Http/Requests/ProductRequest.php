<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {

        return [
            //
            'features_text' => ['array'],
//            'features_text.*.*.value' => ['required','string', 'max:255'],
            'qtu' => ['required'],
            'price' => ['required','numeric'],
            'min_price' => ['required','numeric','max:'.request('price')],
        ];
    }

    public function messages()
    {
       return [
           'features_text.*.*.value.required' => 'Заполните все текстовые особенности',
       ];
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Game;
use Inertia\Inertia;

class MarketController extends Controller
{
    protected $filters = [
        'price' => [
            'min' => null,
            'max' => null,
        ],
    ];
    //
    public function index(Game $game, Category $category){

        $items = \App\Http\Resources\ItemResource::collection(\App\Actions\MarketDataAction::handle($game));

        if(request()->wantsJson()){
            return $items;
        }

        $this->setPrice($items);

        return Inertia::render('Market',[
            'game' => $game,
            'category' => $category,
            'items' => $items,
            'filters' => $this->filters,
        ]);
    }

    protected function setPrice($items){
        foreach ($items->jsonSerialize() as $key => $item){
            if($this->filters['price']['min'] === null){
                $this->filters['price']['min'] = $item['price'];
            }
            if($this->filters['price']['max'] === null){
                $this->filters['price']['max'] = $item['price'];
            }
            if($this->filters['price']['min'] !== null && $item['price'] !== null &&  $item['price'] < $this->filters['price']['min']){
                $this->filters['price']['min'] = $item['price'];
            }

            if($this->filters['price']['max'] !== null && $item['price'] !== null && $item['price'] > $this->filters['price']['max']){
                $this->filters['price']['max'] = $item['price'];
            }

//            if(count($this->filters['categories']) === 0){
//                $this->filters['categories'][] = $item['category']['name'];
//            }else{
//                if(array_search($item['category']['name'], $this->filters['categories']) === false){
//                    $this->filters['categories'][] = $item['category']['name'];
//                }
//            }
        }
    }
}

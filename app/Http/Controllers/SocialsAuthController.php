<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Laravel\Socialite\Facades\Socialite;
use Str;
use Hash;
use Carbon\Carbon;
use Auth;

use App\Models\User;
use App\Mail\SendPasswordForSocAuth;

class SocialsAuthController extends Controller
{
    //Google
    public function googleRedirect(){
        return Socialite::driver('google')->redirect();
    }

    public function googleAuth(){
        $user = Socialite::driver('google')->user();
        $userModel = User::where('email', $user->email);
        $password = $this->generatePassword();
        $hashPassword = Hash::make($password);

        if($userModel->value('vk_id') != null){
            return response()->json(array(
                'code' => 500,
                'message' => 'Данный пользователь зарегестрирован под другой соц сетью'
            ), 500);
        }

        $user = User::updateOrCreate(
            [
                'google_id' => $userModel->value('google_id')
            ],
            [
                'email' => $user->email,
                'nickname' => $user->name,
                'password' => $hashPassword,
                'google_id' => $user->id,
            ]
        );

        $this->checkVerifyMail($user);

        return redirect()->route('dashboard.profile.edit');
    }

    //vk
    public function vkRedirect(){
        return Socialite::driver('vkontakte')->redirect();
    }

    public function vkAuth(){
        try {
            $userSocialite = Socialite::driver('vkontakte')->user();
            $userModel = User::where('email', $userSocialite->email)
                ->orWhere('vk_id',$userSocialite->id)->first();
            $hashPassword = Hash::make($this->generatePassword());

//        if($userModel->google_id != null){
//            return response()->json(array(
//                'code' => 500,
//                'message' => 'Данный пользователь зарегестрирован под другой соц сетью'
//            ), 500);
//        }

            if(!$userModel){
                $userModel = new User();
            }
            $userModel->nickname = $userSocialite->nickname;
            $userModel->password = $hashPassword;
            $userModel->email = $userSocialite->email;
            $userModel->vk_id = $userSocialite->id;

            $this->checkVerifyMail($userModel);

        }catch (\Error $error){
            return response()->json(array(
                'code' => 500,
                'message' => 'Данный пользователь зарегестрирован под другой соц сетью'
            ), 500);
        }
        return redirect()->route('dashboard.profile.edit');
    }

    private function generatePassword(){
        $letters = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz123456789123456789";

        return Str::random(12, $letters).'!';
    }

    private function checkVerifyMail($user){
        $isVerify = !empty($user->emailVerifedAt);

        if($user->value('email_verified_at') == null){
            $user->sendEmailVerificationNotification();
        }

        Auth::login($user);
    }
}

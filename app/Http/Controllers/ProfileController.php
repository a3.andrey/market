<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Str;
use Inertia\Inertia;
use Inertia\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Mail\ChangeEmail;
use App\Models\Notification;


class ProfileController extends Controller
{

    /**
     * Display edit user passowrd
     */
    public function editPasswordView(){
        return Inertia::render('Auth/EditPassword');
    }

    /**
     * Edit user password
     */
    public function editPassword(Request $request){

        if(Hash::check($request->oldPassword, Auth::user()->password)){
            Auth::user()->update([
                'password' => Hash::make($request->password),
            ]);
            $notification = new Notification();
            $notification->createNotification("Вы сменили пароль", "message", route('dashboard.profile.edit'));

            return redirect(route('dashboard.profile.edit'));
        }

        return redirect()->back()
            ->withErrors(['PasswordsDontMatch' => 'Старый пароль введен неверно'])->withInput();

    }


    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): Response
    {
        return Inertia::render('Dashboard/Profile', [
            'mustVerifyEmail' => $request->user() instanceof MustVerifyEmail,
            'user' => $request->user(),
            'status' => session('status'),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(Request $request): RedirectResponse
    {
        //var for update
        $user = Auth::user(); // get now user
        $newEmail = '';
        $code = '';

        //validate
        $req = $request->validate([
            'email' => "required|unique:users,email,$user->id,id",
            'nickname' => ['required'],
            'name' => ['required']
        ]);

        //Check is new email
        if($user->email != $request->email){
            $newEmail = $request->email;
            $code = $this->generateCode();
            $notification = new Notification();

            $user->update([
                'new_email' => $newEmail,
                'verify_email_code' => $code
            ]);

            Mail::to($newEmail)->send(new ChangeEmail($code));
            $notification->createNotification("На ваш email пришла ссылка-подтверждение новой почты", "message", route('dashboard.profile.edit'));
        }

        //updating user
        $user->update([
            'nickname' => $request->nickname,
            'name' => $request->name,
        ]);

        return redirect()->back();
    }

    /**
     * Set new avatar
     */
    public function setNewAvatar(Request $request): RedirectResponse{

        Auth::user()->update([
            'avatar_id' => $request->avatar_id
        ]);

        $notification = new Notification();
        $notification->createNotification('Вы сменили аватарку!', 'message', route(\Request::route()->getName()));

        return redirect()->back();
    }


    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validate([
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    /**
     * Check is user change email
     *
     * @return Bool isHaveNewEmail
     */

    public function checkIsHaveNewEmail(){
        return Auth::user()->new_email != null;
    }

    /**
     * Generate code for verify email
     *
     * @return String $code
     */
    private function generateCode(){
        $letters = "aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz123456789123456789aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz123456789123456789aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz123456789123456789";
        $code = Str::random(12, $letters).'-'.Str::random(12, $letters);

        while($code == User::where('verify_email_code', $code)->value('verify_email_code')){
            $code = Str::random(12, $letters).'-'.Str::random(12, $letters);
        }

        return $code;
    }
}

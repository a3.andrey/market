<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\RedirectResponse;
use Auth;

use App\Models\User;
use App\Models\Notification;

class VerifyEmailController extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     */
    public function __invoke(\App\Http\Requests\Auth\EmailVerificationRequest $request): RedirectResponse
    {

        if((int) request('code') !== session('code')){
            return redirect()->back()->withErrors(["code"=>"Неверный код"]);
        }

//        if ($request->user()->hasVerifiedEmail()) {
//            return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
//        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect()->route('verification.success');
    }

    /**
     * Edit old email to new email
     */

    public function newEmail($code){
        $user = User::where('verify_email_code', $code);
        $notification = new Notification();

        $user->update([
            'email' => $user->value('new_email'),
            'new_email' => null,
            'verify_email_code' => null
        ]);

        $notification->createNotification("Вы сменили почту", "message", route('dashboard.profile.edit'));

        return redirect()->route('games');
    }
}


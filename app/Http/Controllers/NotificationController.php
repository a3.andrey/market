<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Get number of notifications
     *
     * @return Int $notiifcations
     */
    public function count(): Int{
        if(!Auth::check()){
            return 0;
        }

        return Notification::where('user_id', Auth::user()->id)
            ->where('is_check', 0)
            ->get()
            ->count();
    }

    /**
     * @return array
     */

    public function lasts():array
    {
        if(!Auth::check()){
            return [];
        }

        return Notification::where('user_id', Auth::user()->id)->where('is_check', 0)->orderByDesc('id')->limit(3)->get()->toArray();
    }

    /**
     * Send notification
     *
     * @param Request $request - data with description and icon
     */

    public function store(Request $request){
        Notification::createNotification($request->description, $request->icon);
    }

    /**
     * Change notitification check from false to true
     *
     * @param Request $request - data with notification id
     */

    public function check(Request $request){
        Notification::where('id', $request->id)->update([
            'is_check' => 1
        ]);
    }

}

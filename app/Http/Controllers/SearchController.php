<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Services\SearchService;
use cijic\phpMorphy\Facade\Morphy;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public string $search;
    public function __construct()
    {
        $this->search = mb_strtoupper(request('search'), 'utf-8');
    }
    //
    public function __invoke(SearchService $searchService){
        $searchWords = \request('search');
        $searchWordArrays = explode(' ', $searchWords);
        $words = collect();

        foreach ($searchWordArrays as $word){
            if(is_string($word) && $word){
                $words[] = $word;
            }
        }

        foreach ($words as $key=>$item){
            $correct = $searchService->search($item);
            if($correct){
                $words[$key] = $correct;
            }
        }

        $searchResults = null;

        foreach ($words as $word){
            if($searchResults === null){
                $searchResults = Item::where('name', 'LIKE', '%'.$word.'%')->get();
            }else{
                $search = Item::where('name', 'LIKE', '%'.$word.'%')->get();
                $searchResults->merge($search);
            }
        }



        $searchResults->map(function ($item){
            $item->url = route('market.item',[
                $item->game->id,
                $item->categories->first()->id,
                $item->id
            ]);
            return $item;
        });

        return $searchResults;

    }
}


<?php

namespace App\Http\Controllers\UnitPay;

use App\Http\Controllers\Controller;
use App\Models\UnitpayPayment;
use Illuminate\Support\Facades\Http;

class RefundUnitPayController extends Controller
{
    /**
     * @param UnitpayPayment $payment
     * @param $sum
     * @return false|\Illuminate\Http\Client\Response
     */
    public function refundPayment(UnitpayPayment $payment, $sum)
    {
        try {
            $response = Http::get(config('unitpay.pay_api_url'), [
                'method' => 'refundPayment',
                'params[paymentId]' => $payment->unitpay_id,
                'params[secretKey]' => config('secret_key'),
                'params[sum]' => $sum,
            ]);

            if (!$response->successful()) {
                $response->throw();
            }
        } catch (\Exception $e) {
            return false;
        }


        return $response;
    }
}

<?php

namespace App\Http\Controllers\UnitPay;

use App\Http\Controllers\Controller;
use App\Models\UnitpayPayment;
use Illuminate\Support\Facades\Http;

class InfoUnitPayController extends Controller
{
    /**
     * https://help.unitpay.ru/other/test-api#getpayment
     * @param UnitpayPayment $payment
     * @return false|\Illuminate\Http\Client\Response
     */
    public function getPaymentInfo(UnitpayPayment $payment)
    {
        try {
            $response = Http::get(config('unitpay.pay_api_url'), [
                'method' => 'getPayment',
                'params[paymentId]' => $payment->unitpay_id,
                'params[secretKey]' => config('secret_key'),
            ]);

            if (!$response->successful()) {
                $response->throw();
            }
        } catch (\Exception $e) {
            return false;
        }

        return $response;

    }
}

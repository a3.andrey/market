<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'image'
    ];

    /**
     * Get the user's first name.
     */
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => Storage::url($value),
        );
    }

    public function tags(){
        return $this->belongsToMany(GameTag::class,'game_tag','game_id','game_tag_id');
    }

    public function items(){
        return $this->hasMany(Item::class,'game_id');
    }

    public function scopeGetItemsGames(Builder $query): void{
        $items = Item::all();
        $query->whereIn('id',$items->pluck('game_id')->toArray());
    }

}

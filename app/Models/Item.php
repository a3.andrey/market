<?php

namespace App\Models;

use App\Helpers\PriceItemHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use function Clue\StreamFilter\fun;

class Item extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => Storage::url($value),
        );
    }

    protected function price(): Attribute
    {
        return Attribute::make(
            get: fn () => app(PriceItemHelper::class)->handler($this),
        );
    }

    public function products(){
        return $this->hasMany(Product::class,'item_id');
    }

    public function parents(){
        return $this->hasMany(Item::class,'parent_id');
    }

//    public function category(){
//        return $this->belongsTo(Category::class,'category_id');
//    }

    public function categories():BelongsToMany{
        return $this->belongsToMany(Category::class,'item_category','item_id','category_id');
    }

    public function game(){
        return $this->belongsTo(Game::class,'game_id');
    }

    public function parent(){
        return $this->belongsTo(Item::class,'parent_id');
    }

    public function iconFeatures(): HasMany
    {
        return $this->hasMany(ItemFeaturesIcon::class,'item_id');
    }

    public function textFeatures(): HasMany
    {
        return $this->hasMany(ItemFeaturesText::class,'item_id');
    }

    public function colorFeatures(): HasMany
    {
        return $this->hasMany(ItemFeaturesColor::class,'item_id');
    }

    public function numberFeatures(): HasMany
    {
        return $this->hasMany(ItemFeaturesNumber::class,'item_id');
    }

    public function getTextFeaturesItems(){
        return $this->textFeatures->chunk(3);
    }
}

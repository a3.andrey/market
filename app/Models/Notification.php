<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Notification extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'icon',
        'user_id',
        'is_check',
        'link'
    ];

    /**
     * Create one notification to auth user
     *
     * @param string $description Description of notification
     * @param string $icon Name of icon to display
     */
    public function createNotification($description, $icon, $link): Void
    {
        $this->create([
            'description' => $description,
            'icon' => $icon,
            'link' => $link,
            'user_id' => Auth::user()->id,
        ]);
    }

    /**
     * Get the icon associated with the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function icon(): HasOne
    {
        return $this->hasOne(Icon::class, 'icon_id');
    }

    /**
     * Get the user that owns the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

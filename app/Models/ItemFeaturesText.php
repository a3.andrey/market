<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemFeaturesText extends Model
{
    use HasFactory;

    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Storage;

class ItemFeaturesIcon extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Interact with the user's address.
     */
    protected function icon(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Storage::url($value),
        );
    }
}

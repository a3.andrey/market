<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitpayPayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'unitpay_id',
        'account',
        'sum',
        'items_count',
        'user_id',
        'order_id',
        'status',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
}

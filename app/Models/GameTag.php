<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GameTag extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function games(){
        return $this->belongsToMany(Game::class,'game_tag','game_tag_id','game_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Avatar;

use Auth;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nickname',
        'email',
        'new_email',
        'password',
        'google_id',
        'vk_id',
        'avatar_id',
        'name',
        'verify_email_code'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'verify_email_code',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Проверка активный или нет пользователь
     * @return bool
     */
    public function getIsOnlineAttribute()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * Get number of the notifications
     *
     * @return Int
     */

    public function getNumberNotification(){
        return count($this->notifications());
    }


    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function products(){
        return $this->hasMany(Product::class,'user_id');
    }

    public function avatar(){
        return $this->belongsTo(\App\Models\Avatar::class,'avatar_id');
    }


}

const colorGenerator = (color) =>
    "rgba("+color.r+"," +
    " "+color.g+"," +
    " "+color.b+"," +
    " "+color.a+")"

export {colorGenerator}

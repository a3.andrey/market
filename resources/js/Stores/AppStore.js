import {defineStore} from "pinia";

export const UseAppStore = defineStore('UseAppStore',{
    state: () => ({
        // бургер на мобилке с меню
        burgerMobileMenu: false,
        mobileFilterMenu: false,
        // ТАМА САЙТА
        themeSite: localStorage.getItem('themeSite') || 'dark ',
        modalBackground: false,
        modalsActivites: {
            'changeAvatar': false,
            'item': false,
            'deleteLot': false,
            'obrSvaz': false,
            'spor': false,
            'thanks2': false,
            'thanks': false,
        },
        numberOfNotification: 0,
        notifications: [],
        notificationActive:false,
    }),
    getters:{
         noSkroll:() => this.burgerMobileMenu?'noScroll':null
    },
    actions:{
        toggleThemeSite() {
            this.themeSite === 'dark' ? this.themeSite = 'light' : this.themeSite = 'dark'
        },
        toggleActiveModals(modalKey, active){
            this.modalBackground = active;
            this.modalsActivites[modalKey] = active;
        },
        offAllModals(){
            this.modalBackground = false;
            this.modalsActivites= {
                'changeAvatar': false,
                'item': false,
                'deleteLot': false,
                'obrSvaz': false,
                'spor': false,
                'thanks2': false,
                'thanks': false,
            }
        },
        changeNumberNotifications(newNumber){
            this.numberOfNotification = newNumber;
        },
        changeHeaderNotifications(newNotifications){
            this.notifications = newNotifications;
        },
        changeNotificationActive(newValue){
            this.notificationActive = newValue;
        },
        changeFilterMobile(){
            this.mobileFilterMenu = !this.mobileFilterMenu;
        },
        changeBurger(){
            this.burgerMobileMenu = !this.burgerMobileMenu;
        }

    }
})

import {defineStore} from "pinia";

export let UseUserStore = defineStore('UseUserStore', {
    state: () => ({
        avatar: '',
        liked: { }
    }),
    actions: {
        setNewAvatar(newAvatar){
            this.avatar = newAvatar;
        },
        addLiked(idItem, value){
            const item = 'item' + idItem;

            this.liked[item] = value;
            localStorage.setItem(item, value);
        },
        getLike(idItem){
            const item = 'item' + idItem;
            const itemValue = localStorage.getItem(item);

            this.liked[item] = itemValue;

            return itemValue;
        }

    }
})
